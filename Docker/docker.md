# Apuntes de Docker

## `docker run`

Crea e inicia un contenedor a partir de una imagen.

1. Ejemplo de uso más común:

   ```bash
   docker run -d -p 8082:80 docker/getting-started

   # o también
   docker run -dp 8082:80 docker/getting-started
   ```

   Donde:

   * `-d` ejecuta el contenedor en modo "detached" (en segundo plano)
   * `-p` mapea el puerto 8082 del host al puerto 80 del contenedor
   * `docker/getting-started` imagen a usar.

2. Ejemplo usando un punto de montaje vinculado:

   ```bash
   docker run -dp 3000:3000 -w /app -v "$(pwd):/app" node:12-alpine sh -c "yarn install && yarn run dev"
   ```

   Donde:

   * `-d` ejecuta el contenedor en modo "detached" (en segundo plano)
   * `-p` mapea el puerto 3000 del host al puerto 3000 del contenedor
   * __`-w /app` establece la carpeta de trabajo, es decir, la carpeta desde donde se ejecutará el comando `sh`__
   * __`-v "$(pwd):/app"` vincula la carpeta actual del host como punto de montaje para el contenedor dentro de la carpeta `/app`__
   * `node:12-alpine` imagen a usar
   * `sh -c "yarn install && yarn run dev"` el comando a ejecutar. En este caso: inicia un shel usando `sh`, ejecuta `yarn install` para instalar todas las dependencias, y por último, ejecuta `yarn run dev` para ejecutar el/los scripts dev definidos en el `package.json`.

3. Ejemplo enlazando con una red creada:

   ```bash
   docker run -d --network todo-app --network-alias mysql -v todo-mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=todos mysql:5.7
   ```

   Donde:

   * `-e` define variables de entorno.
   * `--network-alias` define un alias o hostname para el contenedor

## Contenedores

### Crear un contenedor desde un Dockerfile

```bash
docker build -t getting-started .
```

Donde:

* `-t` establece la etiqueta o el nombre para el contenedor a crear (getting-started, en este caso)
* `.` indica que el Dockerfile se encuentra en el directorio donde se está invocando docker

### Detener Contenedores

```bash
docker stop <id-o-nombre-del-contenedor> <id-o-nombre-de-otro-contenedor>
```

### Eliminar Contenedores

```bash
# Por nombre o ID
docker rm <id-o-nombre-del-contenedor> <id-o-nombre-de-otro-contenedor>

# Por estatus exited
docker rm $(docker ps -a -f status=exited -q)
```

### Detener y Eliminar Contenedores

```bash
docker rm -f <id-o-nombre-del-contenedor> <id-o-nombre-de-otro-contenedor>
```

### Idiomas y Zonas Horarias

```bash
# Verificar la zona horaria actualmente activa
docker exec <CONTAINER-ID> date

# Verificar el o los idiomas (locale) actualmente admitidos
docker exec <CONTAINER-ID> locale
```

## Imágenes

### Eliminación

```bash
# Eliminación interactiva de imágenes colgadas (que no responden)
docker image prune

# equivalente
docker rmi $(docker images -q -f dangling=true)

# Eliminación de todas las imágenes
(docker rmi $(docker images -a -q)

# Eliminación por un patrón dado
docker images -a | grep "pattern" | awk '{print $3}' | xargs docker rmi
```

### Buscar

```bash
# Listar imágenes por un patrón dado
docker images -a | grep "pattern"

# Encontrar imágenes hijas dependientes
docker inspect --format='{{.Id}} {{.Parent}}' $(docker images --filter since= -q)
```

### Historial de Creación

`history` permite conocer el comando usado para crear cada capa de una imagen.

```bash
docker image history ID-DE-IMAGEN-O-REPOSITORIO

# mostrar salida completa
docker image history --no-trunc ID-DE-IMAGEN-O-REPOSITORIO
```

### Repositorio Docker Hub

#### Iniciar sesión en Docker Hub

```bash
docker login -u NOMBRE-DE-USUARIO
```

### Renombrar Etiquetas de Imágenes

```bash
docker tag etiqueta-original etiqueta-nueva
```

## Volúmenes

### Crear Volúmenes Nombrados

_Los volúmenes nombrados se pueden crear en tiempo de ejecución usando el parámetro `-v` sin necesidad de crearlos previamente._

```bash
docker volume create NOMBRE-DEL-VOLUMEN
```

### Usar Volúmenes Nombrados

```bash
docker run -dp 3000:3000 -v todo-db:/etc/todos getting-started
```

### Inspeccionar Volúmenes

```bash
docker volume inspect NOMBRE-DEL-VOLUMEN
```

## Redes

> If two containers are on the same network, they can talk to each other. If they aren’t, they can’t.

### Listar Redes

```bash
docker network ls
```

### Crear Redes

```bash
docker network create NOMBRE-DE-LA-RED
```

## Inspección de Objetos

### IP de un Contenedor

```bash
$ docker inspect -f \
'{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
<CONTAINER-ID>
```

### Todas las IP de una Red

```bash
docker network inspect -f '{{json .Containers}}' <NETWORK-ID> | jq '.[] | .Name + ":" + .IPv4Address'
```

## HERRAMIENTAS DE DESARROLLO

### PHP8 + Apache HTTP Server

```bash
docker run -d -p 80:80 --name php-8-apache -v "$PWD":/var/www/html php:8-apache
docker stop php-8-apache
```

### PHP8 + Composer

_Para ejecutar instrucciones de composer debe estar en el directorio raíz del proyecto, que es donde debería estar el archivo `composer.json`:_

```bash
cd ruta/al/directorio/raíz/del/proyecto
```

#### Composer: uso básico

__La carpeta vendor heredará los permisos de root.___

```bash
docker run --rm -it -v $PWD:/app composer <command>
sudo chown -R usuario:grupo vendor/
```

#### Composer: caché persistente/configuración global

```bash
docker run --rm -it -v $PWD:/app -v ${COMPOSER_HOME:-$HOME/.composer}:/tmp composer <command>
sudo chown -R usuario:grupo vendor/
```

#### Composer: ejecutar con permisos del usuario actual

```bash
docker run --rm -it -v $PWD:/app -u $(id -u):$(id -g) composer <command>
```

## Monitor

```bash
docker stats
```

## Logs

```bash
docker logs -f ID-O-NOMBRE-DEL-CONTENEDOR
```

## POSTGRESQL

### Restaurar .sql

```bash
docker exec -i <CONTAINER> psql -U <USER> -d <DB-NAME> < <PATH-TO-DUMP>
```
