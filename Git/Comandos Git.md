# Comandos `git`

## Configuración Inicial

### Origen y Configuraciones Actuales

```bash
git config --list --show-origin
```

### Estableciendo Identidad

```bash
git config --global user.name "Nombre(s) Apellido(s)"
git config --global user.email micorreo@dominio.com
```

### Estableciendo Editor

```bash
git config --global core.editor emacs
```

### Estableciendo Colores

```bash
git config --global color.ui auto
```

### Estableciendo Plantilla

```bash
git config --global commit.template ruta/a/la/plantilla.txt
```

### Almacenando Credenciales

```bash
# ATENCIÓN guardado en un archivo txt
git config --global credential.helper store

# Almacena las credenciales por hasta 15 minutos
git config --global credential.helper cache
```

## Creando Repos

```bash
# Inicializa el repo
git init

# Agrega todos los archivos actuales
git add .

# Confirma la inicialización con el primer commit
git commit -m "Inicializa el proyecto"
```

## Ramas (Branches)

### Cambiar/Conmutar Ramas

```bash
git checkout ＜branchname＞
```

### Establecer Nombre por Defecto para Ramas Nuevas

```bash
git config --global init.defaultBranch [BRANCH_NAME]

# ejemplo
git config --global init.defaultBranch main
```

### Actualizar Ramas Remotas en Local

```bash
# Elimina las ramas locales que no existan en el repo remoto
git remote update origin --prune
```

### Deshaciendo Commits

```bash
# Retorceder al git inmediato anterior y mantener los cambios ejecutados
# Recomendado ya que deshace el commit pero permite ver los cambios que
# se ejecutaron
git reset --soft HEAD~1

# Retorceder al git inmediato anterior y eliminar los cambios ejecutados
git reset --hard HEAD~1

# Confirma la inicialización con el primer commit
git commit -m "Inicializa el proyecto"
```

### Deshaciendo Cambios

```bash
# Descartar todos los cambios en la carpeta actual
git restore .

# Descartar todos los cambios en todas las ramas
git restore :/
```

### Deshacer Cambios por Modos de Archivos

```bash
git diff -p -R --no-ext-diff --no-color \
    | grep -E "^(diff|(old|new) mode)" --color=never  \
    | git apply
```

### Exportar rama como archivo ZIP

```bash
git archive --format zip --output /full/path/to/zipfile.zip ＜branchname＞
```
