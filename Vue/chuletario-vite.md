# Vite

## Creando Proyectos

```bash
# De manera asistida
npm init vite@latest

# De manera directa con npm 6.x
npm init vite@latest nombre-del-proyecto --template vue

# De manera directa con npm 7.x
npm init vite@latest nombre-del-proyecto -- --template vue

cd nombre-del-proyecto
npm install
npm run dev
```
