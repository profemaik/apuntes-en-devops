# Vue CLI

## Instalando

```bash
npm install -g @vue/cli
```

## Actualizando

```bash
# Actualiza la installación globa de Vue CLI
npm install -g @vue/cli

# Actualiza uno o más paquetes realcionados con @vue/cli
vue upgrade [opciones] [nombre-del-paquete]
```

## Creando Proyectos

```bash
vue create nombre-del-proyecto

# Usando la GUI
vue ui
```
