# My Chuletario de Node.js

## `npm init`

* Obtener toda la configuración actual de npm

    ```bash
    npm config list
    ```

* Obtener la configuración actual de solamente `init`

    ```bash
    npm config list | grep init
    ```

* Establecer la configuración por defecto de `init`

    ```bash
    npm set init.author.name "Tu nombre"
    npm set init.author.email "tu@email.com"
    npm set init.author.url "https://tu-url.com"
    npm set init.license "MIT"
    npm set init.version "1.0.0"
    ```

## Creando Proyectos

### Configuración Inicial del Proyecto

1. Establecer el archivo de la licencia

    ```bash
    # para una licencia MIT sería
    npx license MIT
    ```

2. Establecer el archivo `.gitignore`

    ```bash
    # para proyectos node
    npx gitignore node
    ```

3. Establecer el Pacto de Colaboradores

    ```bash
    # otorga al proyecto un código de conducta para los contribuyentes
    npx covgen email@address.com
    ```

4. Inicializar el proyecto

    ```bash
    # crea el archivo package.json con la configuración por defecto de init (no pregunta por ningún parámetro)
    npm init -y
    ```

5. Desarrollar el proyecto 👨‍💻

## Referencias

* [license](https://www.npmjs.com/package/license)
* [gitignore](https://www.npmjs.com/package/gitignore)
* [plantillas gitignore disponibles](https://github.com/github/gitignore)
* [covgen](https://www.npmjs.com/package/covgen)
