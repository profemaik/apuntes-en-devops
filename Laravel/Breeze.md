# Laravel Breeze

## Crear Nuevo Proyecto Inertia + Vue

### Crear un nuevo proyecto en limpio

```bash
curl -s "https://laravel.build/mi_app?with=pgsql,mailhog" | bash
```

### Instalar Breeze

```bash
composer require laravel/breeze --dev
```

### Instalar el stack de Inertia

```bash
php artisan breeze:install vue
```

### Instalar el stack de Inertia con soporte para SSR

```bash
php artisan jetstream:install inertia --ssr
```

### Finalizar la instalación

1. ```bash
   npm install
   ```

2. ```bash
   npm run dev
   ```

3. ```bash
   php artisan migrate
   ```
